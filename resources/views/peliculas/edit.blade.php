@extends('layouts.app')
@section('content')
<div class="container">
	<h1  class="text-center">Editar pelicula</h1>
	<div class="col-md-12">
			@include('peliculas.form',['pelicula' => $pelicula, 'url' => 'peliculas/'.$pelicula->id, 'method' => 'PATCH'])
    </div>
</div>
@endsection
