@extends('layouts.app')
@section('content')


<div class="container">
<div class = "big-padding text-center blue-grey white-text">
	    <h2>Administraion de Peliculas</h2>
</div> 
<br>
<a href="{{url('peliculas/create')}}" class="btn btn-primary btn-sm">Agregar nueva pelicula</a>	
{!! Form::open(['url' => 'peliculas','method' => 'GET', 'class' => 'navbar-form navbar-right']) !!}
 <div class="form-group">
 <label for="	">Por usuario que lo creo </label>
		{{ Form::select('user', $users) }}
	</div>
  <div class="form-group">
  <label for="	">Por nombre</label>
		{{ Form::text('titulo',null,['class' => 'form-comtrol' , 'placeholder'=>'Nombre pelicula...']) }}
	</div>
	<div class="form-group text-right" >
		<input type="submit" value="Buscar" class="btn btn-success btn-sm">
	</div>	
	<a href="{{url('peliculas')}}" class="btn btn-success btn-sm">Limpiar</a>
 {!! Form::close() !!}
	<br>
	<table class="table table.bordered">
		<thead>
			<tr>
				<td>ID</td>
				<td>Titulo</td>
				<td>Genero</td>
				<td>Director</td>
				<td>Año lanzamiento</td>
				<td>Portada</td>
				<td>Editar</td>
				<td>Eliminar</td>
			</tr>
		</thead>	
		<tbody>
			@foreach ($peliculas as $pelicula)
				<tr>
					<td>{{ $pelicula->id }}</td>
					<td>{{ $pelicula->titulo }}</td>
					<td>{{ $pelicula->genero }}</td>
					<td>{{ $pelicula->director}}</td>
					<td>{{ $pelicula->date_started}}</td>
					<td> <img style="width: 100px" src="{{asset('uploads/')}}/{{$pelicula->id}}.{{$pelicula->ext}}" alt=""></td>
					<td> 
					<a href="{{url('/peliculas/'.$pelicula->id.'/edit')}}">Editar</a>
					</td>
					<td>
					@include('peliculas.delete',['pelicula' => $pelicula])
					</td>
				</tr>
			@endforeach
		</tbody>
	</table>
		{{ $peliculas->links() }}
	</div>
@endsection
