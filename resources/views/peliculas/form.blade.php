 @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif


{!! Form::open(['url' => $url,'method' => $method , 'files' => true]) !!}
    <div class="form-group">
		{{ Form::text('titulo',$pelicula->titulo,['class' => 'form-control', 'placeholder'=>'Nombre......']) }}
	</div>
	<div class="form-group">
		{{  Form::select('genero', array('ciencia_ficion' => 'Ciencia ficcion', 'accion' => 'Accion', 'terror' => 'Terror', 'comedia' => 'Comedia' )) }}
	</div>
	<div class="form-group">
		{{ Form::text('director',$pelicula->director,['class' => 'form-control', 'placeholder'=>'Nombre director']) }}
	</div>
	<div class="form-group">
		{{ Form::date('fecha_lanzamiento', \Carbon\Carbon::now()) }}
	</div>
	<div class="form-group">
		{{Form::file('cover')}}
	</div>
	<div class="form-group text-right" >
		<a href="{{url('/peliculas')}}"> Regresar al listado de peliculas</a>
		<input type="submit" value="Guardar" class="btn btn-primary">
	</div>	
 {!! Form::close() !!}