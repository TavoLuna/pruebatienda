@extends('layouts.app')
@section('content')
<div class="container">
	<h1  class="text-center">Nueva Pelicula</h1>
	<div class="col-md-12">
		@include('peliculas.form',['pelicula' => $pelicula, 'url' => 'peliculas','method' => 'POST'])
	</div>
</div>
@endsection
