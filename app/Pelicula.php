<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pelicula extends Model
{
    public function scopeTitulo($query, $titulo){
    	if($titulo == null){
        		$query->orderBy('id');
    	}
    	else{
    		$query->where('titulo',$titulo);
    	}  		
    } 

    public function scopeUser($query, $user){
    	if($user == null){
        		$query->orderBy('id');
    	}else{
    		$query->where('user_id',$user);
    	} 				
    }
}
