<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Pelicula;
use App\User;
use Illuminate\Support\Facades\Auth; 
use File;

class PeliculasController extends Controller
{
     public function __construct (){
        $this->middleware("auth");
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $users = User::pluck('name','id');
        $peliculas = Pelicula::titulo($request->get('titulo'))->user($request->get('user'))
                ->paginate(4);
        return view('peliculas.index',[ 'peliculas' => $peliculas, 'users' => $users]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $pelicula = new Pelicula;
        return view("peliculas.create",["pelicula" => $pelicula]); 
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

       $v = \Validator::make($request->all(), [
            
            'titulo' => 'required',
            'genero' => 'required',
            'director' => 'required',

       
        ]);
 
        if ($v->fails())
        {
            return redirect()->back()->withInput()->withErrors($v->errors());
        }
          $hasFile = $request->hasFile('cover') && $request->cover->isValid();

          $pelicula = new Pelicula;
          $pelicula->titulo = $request->titulo;
          $pelicula->genero = $request->genero;
          $pelicula->director = $request->director;
          $pelicula->user_id = Auth::user()->id;
          $pelicula->date_started = $request->fecha_lanzamiento;
           if($hasFile){
                $extension = $request->cover->extension();
                $pelicula->ext = $extension;
             }

             if($pelicula->save()){
                if($hasFile){
                     $request->cover->storeAs('public',"$pelicula->id.$extension");
                     $new = str_replace("/","\\",public_path()."/uploads/$pelicula->id.$extension"); 
                     $path =  str_replace("/","\\",storage_path("app/public/$pelicula->id.$extension"));
                     File::copy($path,$new);
                }
                return redirect("peliculas");
            }else{
            return view("peliculas.create",["pelicula" => $pelicula]);
        }
}

   /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) 
    {
       $pelicula = Pelicula::find($id);
       return view("peliculas.edit",["pelicula" => $pelicula]); 
   }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

         $v = \Validator::make($request->all(), [
            
            'titulo' => 'required',
            'genero' => 'required',
            'director' => 'required',
          
       
        ]);
 
        if ($v->fails())
        {
            return redirect()->back()->withInput()->withErrors($v->errors());
        }
          $hasFile = $request->hasFile('cover') && $request->cover->isValid();

          $pelicula = Pelicula::find($id);
          $pelicula->titulo = $request->titulo;
          $pelicula->genero = $request->genero;
          $pelicula->director = $request->director;
          $pelicula->user_id = Auth::user()->id;
          $pelicula->date_started = $request->fecha_lanzamiento;
           if($hasFile){
                $extension = $request->cover->extension();
                $pelicula->ext = $extension;
             }

             if($pelicula->save()){
                if($hasFile){
                     $request->cover->storeAs('public',"$pelicula->id.$extension");
                     $new = str_replace("/","\\",public_path()."/uploads/$pelicula->id.$extension"); 
                     $path =  str_replace("/","\\",storage_path("app/public/$pelicula->id.$extension"));
                     File::copy($path,$new);
                }
                return redirect("peliculas");
            }else{
            return view("peliculas.create",["pelicula" => $pelicula]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       Pelicula::destroy($id);
       return redirect('peliculas');
    }
}
