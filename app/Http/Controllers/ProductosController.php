<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Producto;
use App\User;
use Illuminate\Support\Facades\Auth;    
use WsdlToPhp\WsSecurity\WsSecurity;

class ProductosController extends Controller
{

     public function __construct (){
        $this->middleware("auth");
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $users = User::pluck('name','id');
        $productos = Producto::titulo($request->get('titulo'))->user($request->get('user'))
                ->paginate(4);
        return view('productos.index',[ 'productos' => $productos, 'users' => $users]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $producto = new Producto;
        return view("productos.create",["producto" => $producto]); 
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

       $v = \Validator::make($request->all(), [
            
            'titulo' => 'required|max:500',
            'precio' => 'required|numeric',
            'descripcion'    => 'required|max:500'
        ]);
 
        if ($v->fails())
        {
            return redirect()->back()->withInput()->withErrors($v->errors());
        }

          $producto = new Producto;
          $producto->titulo = $request->titulo;
          $producto->precio = $request->precio;
          $producto->descripcion = $request->descripcion;
          $producto->user_id = Auth::user()->id;

         if($hasFile){
                $extension = $request->cover->extension();
                $service->extension = $extension;
             }

             if($service->save()){
                if($hasFile){
                     $request->cover->storeAs('public',"$service->id.$extension");
                     $new = str_replace("/","\\",public_path()."/uploads/$service->id.$extension"); 
                     $path =  str_replace("/","\\",storage_path("app/public/$service->id.$extension"));
                     File::copy($path,$new);
                  
                }
                return redirect("/services");
         }else{
            return view("productos.create",["prodcuto" => $producto]);
        }
}

   /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
       $producto = Producto::find($id);
       return view("productos.edit",["producto" => $producto]); 
   }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $v = \Validator::make($request->all(), [
            
            'titulo' => 'required|max:500',
            'precio' => 'required|numeric',
            'descripcion'    => 'required|max:500'
        ]);

           if ($v->fails())
        {
            return redirect()->back()->withInput()->withErrors($v->errors());
        }
        
        $producto = Producto::find($id);
        $producto->titulo = $request->titulo;
        $producto->precio = $request->precio;
        $producto->descripcion = $request->descripcion;
        if($producto->save()){
            return redirect("productos");
        }else{
            return view("productos.edit",["producto" => $producto]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       Producto::destroy($id);
       return redirect('productos');
    }

    public function soap(){
    $soapHeader = WsSecurity::createWsSecuritySoapHeader('iatai', 'iatai', false, 1459451824, 0, true, true);
        $soapClient = new \SoapClient('http://pruebas.allegraplatform.com/GatewayIatai/PSE?wsdl');
        $soapClient->__setSoapHeaders($soapHeader);
        $soapClient->__soapCall('obtenerBancos',  array());

    }
}
