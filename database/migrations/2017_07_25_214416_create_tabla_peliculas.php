<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablaPeliculas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('peliculas',function(Blueprint $tabla){
            $tabla->increments("id");
            $tabla->integer('user_id')->unsigned()->index();
            $tabla->String('titulo');
            $tabla->String('genero');
            $tabla->String('director');
            $tabla->timestamp('date_started');
            $tabla->String('ext');
            $tabla->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
           Schema::drop('peliculas');
    }
}
